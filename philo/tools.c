#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdio.h>
#include "philo.h"

int	ft_atoi(const char *str)
{
	unsigned long int	res;
	int					k;

	res = 0;
	k = 1;
	while (*str == '\t' || *str == '\n' || *str == '\v'
		|| *str == '\f' || *str == '\r' || *str == ' ')
		++str;
	if (*str == '-' || *str == '+')
		if (*str++ == '-')
			k = -1;
	while (*str != 0 && ('0' <= *str && *str <= '9'))
	{
		res *= 10;
		res += (*str++) - '0';
	}
	if (res > 9223372036854775807 && k == 1)
		return (-1);
	if (res - 1 > 9223372036854775807 && k == -1)
		return (0);
	return (k * res);
}

unsigned long	get_time(void)
{
	unsigned long	res;
	struct timeval	tv;

	gettimeofday(&tv, NULL);
	res = tv.tv_sec * 1000 + tv.tv_usec / 1000;
	return (res);
}

int	is_all_numeric(char *str)
{
	while ('0' <= *str && *str <= '9')
		++str;
	if (*str == 0)
		return (1);
	return (0);
}

void	ft_sleep(unsigned long time)
{
	unsigned long	start;
	unsigned long	now;

	start = get_time();
	now = start;
	while (time > now - start)
	{
		now = get_time();
		usleep(100);
	}
}

void	ft_log(char *color, unsigned long timestamp, int id, char *msg)
{
	printf("%s%6lu %2d %s" C_RESET "\n", color, timestamp, id, msg);
}
