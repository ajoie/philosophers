#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "philo.h"

int	parse_args(t_master *m, int argc, char **argv)
{
	int	i;

	if (argc != 5 && argc != 6)
		return (printf("Wrong number of arguments\n"));
	m->setts[NUMBER_OF_EAT] = -1;
	i = 1;
	while (i < argc)
	{
		if (!is_all_numeric(argv[i]))
			return (printf("Wrong argument: Numeric argument required: %s\n",
					argv[i]));
		m->setts[i - 1] = ft_atoi(argv[i]);
		if (m->setts[i - 1] <= 0)
			return (printf("Wrong argument: %s\n", argv[i]));
		++i;
	}
	return (0);
}

int	malloc_arrays(t_master *m)
{
	m->threads = (t_thread *)malloc(sizeof(pthread_t)
			* m->setts[NUMBER_OF_PHILO]);
	m->forks = (t_fork *)malloc(sizeof(t_fork) * m->setts[NUMBER_OF_PHILO]);
	m->structs = (t_philo *)malloc(sizeof(t_philo) * m->setts[NUMBER_OF_PHILO]);
	if (!m->threads || !m->forks || !m->structs)
		return (printf("Malloc error\n"));
	return (0);
}

void	*philo_god_cycle(t_master *m)
{
	int			i;
	int			c;
	t_philo		*p;

	i = 0;
	c = m->setts[NUMBER_OF_PHILO];
	while (1)
	{
		usleep(100), p = &m->structs[i];
		if (p->eat_count == 0 && p->eat_done == 0)
			p->eat_done = 1, c--;
		if (c == 0)
			return (NULL);
		pthread_mutex_lock(&p->change);
		if (get_time() - p->last_eat > (unsigned long)m->setts[TIME_TO_DIE])
		{
			p->status = STATUS_DEAD;
			ft_log(C_RED, get_time() - p->start, p->id, "died");
			return (NULL);
		}
		pthread_mutex_unlock(&p->change);
		i = (i + 1) % m->setts[NUMBER_OF_PHILO];
	}
	return (NULL);
}

void	init_n_run_philos(t_master *m)
{
	int	i;

	i = 0;
	while (i < m->setts[NUMBER_OF_PHILO])
	{
		pthread_mutex_init(&m->forks[i], NULL);
		pthread_mutex_init(&m->structs[i].change, NULL);
		m->structs[i].id = i + 1;
		m->structs[i].setts = m->setts;
		m->structs[i].first_fork = &m->forks[i];
		m->structs[i].secnd_fork = &m->forks[(i + 1)
			% m->setts[NUMBER_OF_PHILO]];
		m->structs[i].eat_count = m->setts[NUMBER_OF_EAT];
		pthread_create(&m->threads[i], NULL, philo_live_cycle, &m->structs[i]);
		pthread_detach(m->threads[i]);
		i += 1;
	}
	philo_god_cycle(m);
}

int	main(int argc, char **argv)
{
	t_master	m;

	if (parse_args(&m, argc, argv))
		return (1);
	if (malloc_arrays(&m))
		return (1);
	init_n_run_philos(&m);
	free(m.threads);
	free(m.forks);
	free(m.structs);
	return (0);
}
