#ifndef PHILO_H
# define PHILO_H

# include <pthread.h>
# include <sys/time.h>

# define C_RED		"\x1b[31m"
# define C_GREEN	"\x1b[32m"
# define C_YELLOW	"\x1b[33m"
# define C_BLUE		"\x1b[34m"
# define C_MAGENTA	"\x1b[35m"
# define C_CYAN		"\x1b[36m"
# define C_RESET	"\x1b[0m"

typedef pthread_mutex_t	t_fork;
typedef pthread_t		t_thread;

# define NUMBER_OF_PHILO	0
# define TIME_TO_DIE		1
# define TIME_TO_EAT		2
# define TIME_TO_SLEEP		3
# define NUMBER_OF_EAT		4

# define STATUS_DEAD		0
# define STATUS_ALIVE		1

typedef struct s_philo
{
	int				id;
	t_fork			*first_fork;
	t_fork			*secnd_fork;
	int				*setts;
	unsigned long	start;
	unsigned long	last_eat;
	int				eat_count;
	int				eat_done;
	int				status;
	pthread_mutex_t	change;
}	t_philo;

typedef struct s_master
{
	int			setts[6];
	t_thread	*threads;
	t_fork		*forks;
	t_philo		*structs;
}	t_master;

/************************** PHILO ****************************/
void				*philo_live_cycle(void *param);

/************************** TOOLS ****************************/
int					ft_atoi(const char *str);
unsigned long		get_time(void);
void				ft_sleep(unsigned long time);
int					is_all_numeric(char *str);
void				ft_log(char *color, unsigned long timestamp,
						int id, char *msg);

#endif
