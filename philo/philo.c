#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "philo.h"

int	philo_sleep(t_philo *this)
{
	ft_sleep(this->setts[TIME_TO_SLEEP]);
	if (this->status == STATUS_DEAD)
		return (1);
	return (0);
}

int	philo_think(t_philo *this)
{
	if (this->status == STATUS_DEAD)
		return (1);
	ft_log(C_RESET, get_time() - this->start, this->id, "is thinking");
	pthread_mutex_lock(this->first_fork);
	if (this->status == STATUS_DEAD)
	{
		pthread_mutex_unlock(this->first_fork);
		return (1);
	}
	ft_log(C_YELLOW, get_time() - this->start, this->id, "has taken a fork");
	pthread_mutex_lock(this->secnd_fork);
	pthread_mutex_lock(&this->change);
	this->last_eat = get_time();
	pthread_mutex_unlock(&this->change);
	if (this->status == STATUS_DEAD)
	{
		pthread_mutex_unlock(this->first_fork);
		pthread_mutex_unlock(this->secnd_fork);
		return (1);
	}
	ft_log(C_YELLOW, get_time() - this->start, this->id, "has taken a fork");
	return (0);
}

int	philo_eat(t_philo *this)
{
	ft_log(C_GREEN, get_time() - this->start, this->id, "is eating");
	ft_sleep(this->setts[TIME_TO_EAT]);
	pthread_mutex_unlock(this->first_fork);
	pthread_mutex_unlock(this->secnd_fork);
	if (this->status == STATUS_DEAD)
		return (1);
	if (this->eat_count > 0)
		this->eat_count -= 1;
	return (0);
}

void	*philo_live_cycle(void *param)
{
	t_philo			*this;

	this = param;
	this->start = get_time();
	this->last_eat = this->start;
	this->status = STATUS_ALIVE;
	this->eat_done = 0;
	if (this->id % 2 == 0)
		ft_sleep(this->setts[TIME_TO_EAT]);
	if (this->status == STATUS_DEAD)
		return (NULL);
	while (1)
	{
		if (philo_think(this) || philo_eat(this) || philo_sleep(this))
			return (NULL);
	}
	return (NULL);
}
